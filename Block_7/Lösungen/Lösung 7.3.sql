-- Liste aller Produkte mit Produktkategorien
SELECT * FROM category c LEFT JOIN product p ON fk_category_id = c.id; 

-- Liste aller Produktkategorien und Anzahl darin enthaltener Produkte. Zeigen Sie alle Kategorien an und verwenden Sie COUNT().
SELECT c.category, COUNT(p.name) FROM category c LEFT JOIN product p ON fk_category_id = c.id GROUP BY c.category; 


-- Liste der Namen aller Kunden und Anzahl Bestellungen aller Kunden.
SELECT c.firstname, c.lastname, COUNT(oe.id) FROM customer c LEFT JOIN order_entry oe ON oe.fk_customer_id = c.id GROUP BY c.firstname, c.lastname;

-- Liste der Produktkategorien, die keine Produkte enthalten.
SELECT c.category FROM category c LEFT JOIN product p ON p.fk_category_id = c.id WHERE p.id IS NULL;

-- Liste der Produkte, die noch nie bestellt wurden
SELECT * FROM product p LEFT OUTER JOIN product_order_entry poe ON poe.fk_product_id = p.id;