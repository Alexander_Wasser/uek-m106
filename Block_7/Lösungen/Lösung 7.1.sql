SELECT AVG(price) FROM product; -- Durchschnittspreis alle Produkte

SELECT fk_category_id, COUNT(*) FROM product GROUP BY fk_category_id; -- Anzahl Produkte pro Produktkategorie

SELECT fk_zip_id, COUNT(*) FROM CUSTOMER GROUP BY fk_zip_id; -- Anzahl Kunden pro Postleitzahl-ID

SELECT fk_category_id, MAX(price), MIN(price), AVG(price) FROM product GROUP BY fk_category_id; -- Höchst-, Mindest- und Durchschnittspreis aller Produktkategorien.

SELECT AVG(price) FROM product WHERE fk_category_id = 2; -- Durchnschnittspreis der Produktkategorie mit Fremdschlüssel 2

SELECT fk_category_id, AVG(price) FROM product GROUP BY fk_category_id HAVING AVG(price) > 13.04; -- Produktkategorien mit einem Durchschnittspreis grösser als das Resultat von Aufgabe 1