SELECT c.firstname, c.lastname, z.city, z.zip FROM customer c INNER JOIN zip z ON c.fk_zip_id = z.id; -- Adressliste aller Kunden mit Postleitzahl und Ort

SELECT p.description, c.category FROM product p INNER JOIN category c ON fk_category_id = c.id; -- Alle Produkte mit Beschreibung und Name der Kategorie

SELECT c.address, z.zip, oe.ordered_at, oe.delivered_at FROM zip z INNER JOIN customer c ON c.fk_zip_id = z.id INNER JOIN order_entry oe ON oe.fk_customer_id = c.id; -- Alle Bestellungen mit Adresse, Postleitzahl, Bestell- und Lieferdatum

SELECT o.ordered_at, c.email, p.name, po.amount, po.price, po.price*amount AS sum FROM order_entry o INNER JOIN customer c ON o.fk_customer_id = c.id INNER JOIN product_order_entry po ON po.fk_order_entry_id = o.id INNER JOIN product p ON po.fk_product_id = p.id;  -- Alle Bestellungen mit Email des Kunden, Bestelldatum, Produktbezeichnung, Anzahl, Preis und Summe

SELECT c.category, COUNT(p.id) FROM category c INNER JOIN product p ON p.fk_category_id = c.id GROUP BY c.category; -- Kategorienamen mit Anzahl darin enthaltener Produkte

SELECT c.firstname, c.lastname, COUNT(o.fk_customer_id) FROM order_entry o INNER JOIN customer c ON o.fk_customer_id = c.id GROUP BY c.firstname, c.lastname; -- Kundennamen mit Anzahl Bestellungen

UPDATE product p INNER JOIN category c ON p.fk_category_id = c.id SET p.price = p.price * 1.1 WHERE c.category = 'Pizza'; -- Erhöhen Sie den Preis aller Produkte, die einer bestimmten Kategorie zugeordnet sind. Verwenden Sie einen Join im Update Statement.

DELETE oe FROM order_entry oe INNER JOIN customer c ON oe.fk_customer_id = c.id INNER JOIN zip z ON c.fk_zip_id = z.id WHERE z.zip = 8051; -- Löschen Sie alle Bestellungen, die von Kunden an einer bestimmten Postleitzahl getätigt wurden. Verwenden Sie einen Join im Delete Statement.