# Block 6: Daten aus einer SQL Datenbank als CSV exportieren

[TOC]

## Lernziele:
- Ich kann Resultate von Abfragen in CSV Exportieren
- Ich kann einen SQL-Dump einer Datenbank erzeugen
- Ich kann den Inhalt einer CSV Datei in eine Tabelle laden

## Aufgabe 6.1: CSV Dateien exportieren
Lösen Sie die folgenden Aufgaben in Ihrer Sakila Datenbank

- Erstellen Sie eine Liste aller Kunden und speichern Sie sie als CSV Datei ab. Folgende Felder müssen exportiert werden:
1. customer.customer_id
2. customer.first_name
3. customer.last_name
4. customer.email
5. customer.create_date (Format DD.MM.YYYY)
- Öffnen Sie die Adressliste in Excel.
- Wiederholen Sie den Vorgang. Nehmen Sie diesmal den ; als Feldtrenner und " als Feld-Enclosing.