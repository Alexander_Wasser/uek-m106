DROP DATABASE IF EXISTS pizza_express;
CREATE DATABASE pizza_express;
USE pizza_express;

CREATE TABLE zip (
 id INT UNSIGNED NOT NULL AUTO_INCREMENT,
 zip SMALLINT(4) UNSIGNED NOT NULL,
 city VARCHAR(255) NOT NULL,

 PRIMARY KEY(id),
 UNIQUE(zip, city)
);

CREATE TABLE customer (
 id INT UNSIGNED NOT NULL AUTO_INCREMENT,
 fk_zip_id INT UNSIGNED NOT NULL,
 firstname VARCHAR(255) NOT NULL,
 lastname VARCHAR(255) NOT NULL,
 address VARCHAR(255) NOT NULL,
 email VARCHAR(384) NOT NULL UNIQUE,
 password VARCHAR(255) NOT NULL,
 phone VARCHAR(255),

 PRIMARY KEY(id),
 FOREIGN KEY(fk_zip_id) REFERENCES zip(id) -- ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE category (
 id INT UNSIGNED NOT NULL AUTO_INCREMENT,
 category VARCHAR(255) NOT NULL UNIQUE,
 PRIMARY KEY(id)
);

CREATE TABLE product (
 id INT UNSIGNED NOT NULL AUTO_INCREMENT,
 fk_category_id INT UNSIGNED NOT NULL,
 name VARCHAR(255) NOT NULL,
 description VARCHAR(255) NOT NULL,
 price DECIMAL(6,2) NOT NULL,

 PRIMARY KEY(id),
 FOREIGN KEY(fk_category_id) REFERENCES category(id) -- ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE order_entry (
 id INT UNSIGNED NOT NULL AUTO_INCREMENT,
 fk_customer_id INT UNSIGNED NOT NULL,
 ordered_at DATETIME NOT NULL DEFAULT current_timestamp,
 delivered_at DATETIME NULL,

 PRIMARY KEY(id),
 FOREIGN KEY(fk_customer_id) REFERENCES customer(id) -- ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE product_order_entry (
 id INT UNSIGNED NOT NULL AUTO_INCREMENT,
 fk_product_id INT UNSIGNED NOT NULL,
 fk_order_entry_id INT UNSIGNED NOT NULL,
 amount INT UNSIGNED NOT NULL DEFAULT 1,
 price DECIMAL(6,2) NULL,

 PRIMARY KEY(id),
 FOREIGN KEY(fk_product_id) REFERENCES product(id), -- ON UPDATE CASCADE ON DELETE CASCADE,
 FOREIGN KEY(fk_order_entry_id) REFERENCES order_entry(id) -- ON UPDATE CASCADE ON DELETE CASCADE
);


-- Sie wollen in der Kundentabelle auch die mobile Telefonnummer speichern. Fügen Sie eine entsprechende Spalte hinzu
ALTER TABLE customer ADD mobile VARCHAR(255);

-- Ändern Sie den Namen der Spalte für die Produktbezeichung
ALTER TABLE product CHANGE name product_name VARCHAR(255) NOT NULL;
ALTER TABLE product CHANGE product_name name VARCHAR(255) NOT NULL;

-- Ändern Sie den Datentyp des Produktpreises auf DECIMAL(6,2) UNSIGNED
ALTER TABLE product MODIFY price DECIMAL(6,2) UNSIGNED;

-- Setzten Sie nachträglich NOT NULL für den Produktpreis
ALTER TABLE product MODIFY price DECIMAL(6,2) NOT NULL;

-- Fügen Sie ein neues Attribut (created_at, DATETIME) in die Produkttabelle ein und stellen Sie sicher, dass dieses Feld automatisch mit dem aktuellen Zeitpunkt bei einem INSERT befüllt wird.
ALTER TABLE product ADD created_at DATETIME NOT NULL DEFAULT NOW();

-- Entfernen Sie die Spalte für die mobile Telefonnummer wieder
ALTER TABLE customer DROP mobile;

-- Entfernen Sie den Foreign Key CONSTRAINT vom Postleitzahlen Fremdschlüssel aus der Kundentabelle
SHOW CREATE TABLE customer; -- Name des CONSTRAINTs herausfinden (z.B. customer_ibfk_1)
ALTER TABLE customer DROP CONSTRAINT customer_ibfk_1;

-- Fügen Sie den Foreign Key CONSTRAINT wieder hinzu
ALTER TABLE customer ADD foreign key(fk_zip_id) REFERENCES zip(id);