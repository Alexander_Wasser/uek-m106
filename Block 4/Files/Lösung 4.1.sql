SELECT * FROM product; -- Liste aller Produkte

SELECT * FROM category; -- Liste aller Kategorien

SELECT firstname, lastname, email FROM customer; -- Liste aller Kunden. Geben sie nur Vorname, Nachname und Emailadresse aus

SELECT * FROM order_entry WHERE id >= 1 ORDER BY ordered_at ASC; -- Liste alles Bestellungen sortiert nach Bestelldatum

SELECT * FROM prudct WHERE id >= 1 ORDER BY price DESC; -- Liste aller Produkte absteigend sortiert nach Preis

SELECT * FROM product ORDER BY price DESC LIMIT 3; -- Liste der teuersten 3 Produkte

SELECT * FROM product ORDER BY price ASC LIMIT 3; -- Liste der günstigsten 3 Produkte

