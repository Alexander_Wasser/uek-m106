SELECT * FROM customer WHERE firstname LIKE "f%"; -- Kunden deren Vorname mit "f" beginnt

SELECT * FROM customer WHERE lastname LIKE "%r"; -- Kunden deren Nachname mit dem buchstaben "r" endet.

SELECT * FROM customer WHERE lastname LIKE "%e%"; -- Kunden deren Nachname ein "e" enthält

SELECT * FROM customer WHERE firstname LIKE "_____"; -- Kunden deren vorname aus 5 Buchstaben besteht

SELECT * FROM customer WHERE firstname LIKE "%e_"; -- Kunden deren Nachname an der zweitletzter Stelle ein "e" haben