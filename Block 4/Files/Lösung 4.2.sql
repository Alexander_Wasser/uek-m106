SELECT SQRT(price) price FROM product; -- Berechnen Sie die Quadratwurzel aller Produktpreise.

SELECT MONTHNAME (ordered_at) FROM order_entry; -- Geben Sie den Namen des Monats aus dem Datum der Bestellungen aus.

SELECT CHARACTER_LENGTH (firstname) FROM customer; -- Zählen Sie die Anzahl Buchstaben in den Vornamen der Kunden.

SELECT SUBSTRING_INDEX(email, "@", 1) AS Account, SUBSTRING_INDEX(email, "@", -1) AS Domain FROM customer; -- Liste der Email Adressen aller Kunden. Teilen Sie die Adresse in zwei Spalten auf Account und Domain. z.B. hans@muster.com: hans, muster.com

SELECT CONCAT(LEFT(firstname, 1), LEFT(lastname,1 )) AS Initials FROM customer; -- Geben Sie die Initialen der Kunden in einer Spalte aus. z.B. Hans Muster: HM

SELECT price, round(price / 108 * 8, 2) FROM product;       SELECT price, round(round(price / 108 * 8 * 2, 1) / 2, 2) FROM product; -- Berechnen Sie die 8% Mehrwertsteuer, die in den Preisen inbegriffen ist (Optional: Runden Sie die MwSt auf 5 Rappen)

SELECT count(*) FROM product; -- Geben Sie die Anzahl Datensätze ihrer Produkttabelle aus.

SELECT min(price), max(price), avg(price) FROM product; -- Berechnen Sie Mindest-, Höchst- und Durchschnittspreis aller Produkte