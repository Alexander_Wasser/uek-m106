SELECT * FROM product WHERE id = 5; -- Produkt mit dem Primärschüssel 5

SELECT * FROM customer WHERE id < 3; -- Kunden deren Primärschlüssel kleiner ist als 3

SELECT * FROM customer WHERE id < 3 OR id > 8; -- Kunden deren Primärschlüssel kleiner ist als 3 oder  grösser als 8.

SELECT * FROM order_entry WHERE id >= 3 AND id <= 7; -- Bestellungen mit Primärschlüssel zwischne 3 und 7.

SELECT * FROM customer WHERE id = 1 OR id = 3 OR id = 5 OR id = 6; -- Kunden mit den Primärschlüsseln 1,3,5 und 6

SELECT * FROM order_entry WHERE delivered_at IS NULL; -- Bestellungen deren Lieferdatum NULL ist

SELECT * FROM prodcut WHERE price > (SELECT AVG(price) FROM product); -- Produkte die mehr kosten als der Durchschnitt
