-- Name und Adresse des Mitarbeiters der im August des letzten Jahres den höchsten Umsatz erzielt hat
SELECT
 first_name,
 last_name,
 address,
 SUM(amount)
FROM
 staff
JOIN address USING(address_id)
JOIN payment USING(staff_id)
WHERE
 mONth(payment_date) = 8 AND
 year(payment_date) = 2021
ORDER BY
 SUM(amount) DESC
LIMIT
 1
;

-- Umsätze der beiden Filialen mit Adresse und dem Namen des verantwortlichen Managers
SELECT
 address,
 city,
 first_name,
 last_name,
 SUM(amount)
FROM
 store
JOIN address USING(address_id)
JOIN city USING(city_id)
JOIN staff ON manager_staff_id = staff_id
JOIN payment USING(staff_id)
GROUP BY
 store.store_id
;

-- Die 10 Kunden mit dem höchsten Umsatz
SELECT
 first_name,
 last_name,
 SUM(amount)
FROM
 customer
JOIN payment USING(customer_id)
GROUP BY
 customer_id
ORDER BY
 SUM(amount) DESC
LIMIT
 10
;

-- Liste der Städte, in denen mehr als 1 Kunde wohnt
SELECT
 city,
 COUNT(*)
FROM
 customer
JOIN address USING(address_id)
JOIN city USING(city_id)
GROUP BY
 city_id
having
 COUNT(*) > 1
;

-- Name des Schauspielers, der in den meisten Filmen mitgespielt hat
SELECT
 first_name,
 last_name,
 COUNT(*)
FROM
 actor
JOIN film_actor USING(actor_id)
GROUP BY
 actor_id
ORDER BY
 COUNT(*) DESC
LIMIT
 1
;

-- Alternative mit Subquery und MAX()
SELECT
 first_name,
 last_name,
 COUNT(*)
FROM
 actor
JOIN film_actor USING(actor_id)
GROUP BY
 actor_id
having
 COUNT(*) = (
  SELECT
   MAX(c)
  FROM
   (
    SELECT
     COUNT(*) c
    FROM
     actor
    JOIN film_actor USING(actor_id)
    GROUP BY
     actor_id
   ) AS temp
 )
;

-- Name des Filmes, in welchem die meisten Schauspieler mitspielen
SELECT
 title,
 COUNT(*)
FROM
 film
JOIN film_actor USING(film_id)
GROUP BY
 film_id
ORDER BY
 COUNT(*) DESC
LIMIT
 1
;

-- Alternative mit Subquery und MAX()
SELECT
 title,
 COUNT(*)
FROM
 film
  JOIN film_actor USING(film_id)
GROUP BY
 film_id
having
 COUNT(*) = (
  SELECT
   MAX(c)
  FROM
   (
    SELECT
     COUNT(*) c
    FROM
     film
    JOIN film_actor USING(film_id)
    GROUP BY
     film_id
   ) AS temp
 )
;

-- Durchschnittliche Anzahl Schauspieler pro Film
SELECT
 AVG(c)
FROM
 (
  SELECT
   COUNT(*) c
  FROM
   film
  JOIN film_actor USING(film_id)
  GROUP BY
   film_id
 ) AS temp
;

-- Füllen Sie die Kategorie "Art" mit allen Filmen in denen "Matthew Carrey" oder "Jessica Bailey" mitspielen
INSERT INTO
 film_category (category_id, film_id)
SELECT DISTINCT
 (SELECT category_id FROM category WHERE name = "Art"),
 film_id
FROM
 film
JOIN film_actor USING(film_id)
JOIN actor USING(actor_id)
WHERE
 (first_name = "Matthew" AND last_name = "Carrey") OR
 (first_name = "Jessica" AND last_name = "Bailey")
;

-- Löschen Sie alle Filme in welchen "Warren Nolte" mitspielt
SET foreign_key_checks = 0;
DELETE
 rental,
 inventory,
 film_category,
 film_actor,
 film
FROM
 actor
JOIN film_actor USING(actor_id)
JOIN film_category USING(film_id)
JOIN inventory USING(film_id)
JOIN film USING(film_id)
JOIN rental USING(inventory_id)
WHERE
 first_name = "Warren" AND
 last_name = "Nolte"
;
SET foreign_key_checks = 1;

-- Ein Kunde Fragt, ob der Film "CREATURES SHAKESPEARE" im Store in Lethbridge verfügbar ist oder ob alle Exemplare bereits ausgeliehen sind. Schreiben Sie ein Query, das diese Frage beantworten kann
SELECT
 title,
 city,
 COUNT(DISTINCT inventory_id) AS Exemplare,
 COUNT(*) AS Ausleihen,
 SUM(return_date IS NOT NULL) AS Retour,
 SUM(return_date IS NULL) AS Offen,
 COUNT(DISTINCT inventory_id) - SUM(return_date IS NULL) AS 'Verfügbar'
FROM
 film
JOIN inventory USING(film_id)
JOIN store USING(store_id)
JOIN address USING(address_id)
JOIN city USING(city_id)
JOIN rental USING(inventory_id)
WHERE
 title = "CREATURES SHAKESPEARE" AND
 city = "Lethbridge"
GROUP BY
 film_id,
 store_id
;
