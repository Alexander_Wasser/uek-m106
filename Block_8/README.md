# Block 8: Komplexe SQL Statements entwickeln

[TOC]

## Lernziele
- Ich kann mich in eine fremde Datenbank einarbeiten
- Ich kann komplexe SQL Statements entwickeln

## Aufgabe 8.1: Komplexe SQL Queries
Verwenden Sie für diese Aufgabe die Datenbank 'sakila' und schreiben Sie die folgenden SQL Queries:

- Adressliste der Kunden mit Adresse, Stadt und Land
- Vorname und Nachname aller Schauspieler des Films "HARRY IDAHO"
- Namen aller Kategorien und Anzahl Filme
- Namen der Kategorien die weniger als 60 Filme enthalten
- Länder mit mehr als 30 Städten
- Durchschnittliche Länge der Filme in der Kategorie "Comedy", welche "PG-13" geratet sind
- Umsatz des Unternehmens (Payments) mit Filmen, die als "NC-17" geratet wurden
- Tammy Sanders aus Changhwa hat alle Filme zurückgebracht. Erfassen Sie alle notwendigen Änderungen in der Datenbank
- Entfernen Sie alle Filme, in welchen "Dan Torn" mitspielt, aus der Kategorie Drama
- Titel der Filme mit der längsten Spieldauer

### Lösung
