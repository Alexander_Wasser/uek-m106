# Block 2: Repetition Schema entwerfen und Tabellen anlegen

[TOC]

## Lernziele
- Ich kann ein Datenbank Schema in der 3. Normalform entwerfen
- Ich kenne dies SQL Befehle die zur Data Definition Language (DDL) gehören 
- Ich kann Datenbanken und Tabellen mit SQL erzeugen
- Ich kann Datenbanken und Tabellen mit SQL löschen
- Ich kann die Tabellenstruktur mit SQL ändern

## Aufgabe 2.1: ERM entwerfen
Ihre Aufgabe ist es, eine neue Datenbank für einen online Shop zu erstellen. Sie können selber wählen, welche Produkte ihr Shop vertreibt. Wenn nötig, nehmen Sie zur Lösung aus Lesen: Theorie zu Entity Relationship Models (ERM).

**Beispiele:**

- Pizzakurier
- Kleidershop
- Autozubehör

**Funktionalität**
Ihr Webshop muss in der Lage sein, mindestens die folgenden Daten zu speichern:

- Produkte mit Bezeichnung, Beschreibung, Preis
Produktkategorien
- Kunden mit Name, Adresse, Postleitzahl, Ort, Email und Telefonnummer
- Bestellungen mit Produkten, Anzahl, Bestell- und Lieferdatum
Hinweis: Das Lieferdatum kann NULL Werte enthalten.


**Aufgaben**
1. Zeichnen Sie das ERM in der Chen-Notation von Hand oder auf https://www.draw.io

### Lösung
Hier finden sie meine erstelle [Lösung](./Files/Unbenanntes%20Diagramm.drawio)

## Aufgabe 2.2: Datentypen richtig einsetzen
Dies ist ein Versuchstest. 

Das ist die Lösung:
<img src="./Images/Screenshot 2024-03-13 143138.png" alt="drawing" width="300px"/>

## Aufgabe 2.3: DBM erstellen
Setzen Sie dieses Datenbankmodell mit SQL um.

<img src="./Images/dbm-pizza_express.png" alt="drawing" width="300px"/>


Hier ist mein Code: 
```
-- Primärschlüssel am Ende der Tabellendefinition
-- Auomatische Primärschlüssel definieren mit AUTO_INCREMENT
-- UNSIGNED bei INT definiert, dass in diesem Attribut nur positive Zahlen abgelegt werden können
-- Primärschlüssel und Fremdschlüssel müssen die Datentypen aufweisen
-- Mit NOT NULL kann ich einzelne Attribute zu Pflichteldern machen

-- Habe ich beim Erstellen der Tabelle am Ende jeder Zeile bei den Attributen ein Koma?
-- Habe ich am Ende eines Befehls/Statement ein Semikolon?

-- Löscht Datenbank, falls sie existiert
DROP DATABASE IF EXISTS pizzaexpress;

-- Erstellt eine neue Datenbank
CREATE DATABASE pizzaexpress;

-- Mit der Datenbank verbinden
USE pizzaexpress;

-- Löscht Tabelle, wenn existiert
DROP TABLE IF EXISTS order_entry;

-- Kreiert Tabelle
CREATE TABLE order_entry (
    id INT PRIMARY KEY,
    fk_customer_id INT,
    ordered_at DATETIME,
    delivered_at DATETIME
);

DROP TABLE product_order_entry;

CREATE TABLE product_order_entry (
    id INT PRIMARY KEY,
    fk_product_id INT,
    fk_order_entry_id INT,
    amount INT
);

DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    id INT PRIMARY KEY,
    fk_zip_id INT, 
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    address VARCHAR(255),
    email VARCHAR(384),
    password VARCHAR(255),
    phone VARCHAR(255)
);

DROP TABLE IF EXISTS zip;

CREATE TABLE zip (
    id INT PRIMARY KEY,
    zip INT(4),
    city VARCHAR(255)
);

DROP TABLE IF EXISTS category;

CREATE TABLE category (
    id INT PRIMARY KEY,
    category VARCHAR(255)
);

DROP TABLE IF EXISTS product;

CREATE TABLE product (
    id INT PRIMARY KEY,
    fk_product_id INT,
    name VARCHAR(255),
    description VARCHAR(255),
    price DECIMAL(6,2)
);

ALTER TABLE customer ADD mobile VARCHAR(255);
```