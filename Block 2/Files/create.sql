-- Primärschlüssel am Ende der Tabellendefinition
-- Auomatische Primärschlüssel definieren mit AUTO_INCREMENT
-- UNSIGNED bei INT definiert, dass in diesem Attribut nur positive Zahlen abgelegt werden können
-- Primärschlüssel und Fremdschlüssel müssen die Datentypen aufweisen
-- Mit NOT NULL kann ich einzelne Attribute zu Pflichteldern machen

-- Habe ich beim Erstellen der Tabelle am Ende jeder Zeile bei den Attributen ein Koma?
-- Habe ich am Ende eines Befehls/Statement ein Semikolon?

-- Löscht Datenbank, falls sie existiert
DROP DATABASE IF EXISTS pizzaexpress;

-- Erstellt eine neue Datenbank
CREATE DATABASE pizzaexpress;

-- Mit der Datenbank verbinden
USE pizzaexpress;

-- Löscht Tabelle, wenn existiert
DROP TABLE IF EXISTS order_entry;

-- Kreiert Tabelle
CREATE TABLE order_entry (
    id INT PRIMARY KEY,
    fk_customer_id INT,
    ordered_at DATETIME,
    delivered_at DATETIME
);

DROP TABLE product_order_entry;

CREATE TABLE product_order_entry (
    id INT PRIMARY KEY,
    fk_product_id INT,
    fk_order_entry_id INT,
    amount INT
);

DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    id INT PRIMARY KEY,
    fk_zip_id INT, 
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    address VARCHAR(255),
    email VARCHAR(384),
    password VARCHAR(255),
    phone VARCHAR(255)
);

DROP TABLE IF EXISTS zip;

CREATE TABLE zip (
    id INT PRIMARY KEY,
    zip INT(4),
    city VARCHAR(255)
);

DROP TABLE IF EXISTS category;

CREATE TABLE category (
    id INT PRIMARY KEY,
    category VARCHAR(255)
);

DROP TABLE IF EXISTS product;

CREATE TABLE product (
    id INT PRIMARY KEY,
    fk_product_id INT,
    name VARCHAR(255),
    description VARCHAR(255),
    price DECIMAL(6,2)
);

ALTER TABLE customer ADD mobile VARCHAR(255);