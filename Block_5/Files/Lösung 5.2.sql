SELECT * FROM payment; -- Anzahl Datensätze in der Tabelle payment

SELECT * FROM actor WHERE first_name LIKE "julia"; -- Anzahl Schauspieler mit dem Namen Julia

SELECT * FROM customer WHERE active = 0; -- Anzahl inaktive Kunden

SELECT avg(length) FROM film WHERE rating LIKE "PG"; -- Durchschnittliche Läge der Filme mit Rating "PG"

SELECT * FROM rental WHERE return_date IS NULL ORDER BY rental_date ASC; -- Ausleihen, die noch nicht zurück gebracht wurden, sortiert nach Ausleihdatum (Formatdd.mm.YYYY)

SELECT AVG(DATEDIFF(return_date, rental_date)) FROM rental; -- Berechnen Sie die durchschnittliche Ausleihdauer in Tagen