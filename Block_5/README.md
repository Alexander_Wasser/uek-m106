# Block 5:Datenbanken analysieren und abfragen

[TOC]

## Lernziele
- Ich kann einen SQL Dump auf einen Datenbankserver einspielen
- Ich kann mich in eine unbekannte Datenbank einarbeiten

## Voraussetzung: Übungsdatenbank "Sakila" importieren
Hier gelangen sie zu der [Sakila Datenbank](/Block%205/sakila/)

## Aufgabe 5.1 Datenbank analysieren
Importieren Sie den SQL-Dump (siehe Vorbereitung dieses Blocks) der Datenbank Sakila auf ihren MariaDB Server und analysieren Sie die Datenbank. Beantworten Sie folgende Antworten:

*Hinweiss: Verwenden sie dafür die SQL Befehle DESCRIBE und SELECT sowie das Datenbank-Schema (png/mwb) um die Fragen zu beantworten.*

- Welche Tabellen sind in der Datenbank vorhanden und wie sind diese mit Primär- und Fremdschlüssel verbunden?
- Was wird in dieser Datenbank reps. in den einzelnen Tabellen gespeichert?
- Was ist der Umfang der in der Datenbank gespeicherten Daten?
- Welchen Geschäftsprozess bildet die Datenbank Sakila ab?

## Aufgabe 5.2: SQL Abfragen machen
Schreiben Sie die folgenden SQL Queries:

- Anzahl Datensätze in der Tabelle payment
- Anzahl Schauspieler mit dem Namen Julia
- Anzahl inaktive Kunden
- Durchschnittliche Länge der Filme mit Rating "PG"
- Ausleihen, die noch nicht zurück gebracht wurden, sortiert nach Ausleihdatum (Format: dd.mm.YYYY)
- Berechnen Sie die durchschnittliche Ausleihdauer in Tagen
- Liste der Vornamen von Schauspielern, deren Vorname nur 3 Buchstaben lang ist. Zeigen Sie keine doppelten Vornamen an
- Erstellen Sie eine Liste der Schauspieler, deren Nachname mit 'B' beginnt und an zweitletzter Stelle ein 'e' haben. Zeigen Sie keine doppelten Namen an
- Zählen Sie bei allen Datensätzen der Tabelle "rental" 12 Jahre zu Ausleih- und Rückgabedatum dazu
- Erstellen Sie eine neue Kategorie namens "Art"